﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LabWebBrowser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            wb.ScriptErrorsSuppressed = true;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            wb.GoBack();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            wb.GoForward();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            wb.Refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            wb.Stop();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            wb.Navigate(link.Text);
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            link.Text = wb.Url.ToString();
           
        }

        private void link_KeyPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                wb.Navigate(link.Text);
            }
        }

        private void Open_Click(object sender, EventArgs e)
        {
           wb.Navigate(@"C:\Users\174-332\source\repos\LabWebBrowser\LabWebBrowser\potato.html");
        }

        private void open2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openit = new OpenFileDialog();
            openit.Filter = "Html-files (*.html)|*.html";
            openit.FilterIndex = 1;
            openit.RestoreDirectory = true;
            try
            {
                openit.ShowDialog();
                wb.Navigate(openit.FileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Вы выбрали нечитаемый файл, пожалуйста, попробуйте еще раз. " + ex.Message);
            }
        }
    }
}
