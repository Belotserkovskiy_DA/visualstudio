﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabStruktura
{
    class Program
    {
        struct client
        {
            public string fio, naz, prof;
        }
        static void Main(string[] args)
        {
           
            Queue och;
            och = new Queue();
            client a1;
                a1.fio = "Балтиев Иван Михайлович";
                a1.naz = "Грузин";
                a1.prof = "Президент";
            och.Enqueue(a1);
            client a2;
                a2.fio = "Алёхин Евгений Степанович";
                a2.naz = "Русский";
                a2.prof = "Секретарь";
            och.Enqueue(a2);
            client a3;
            a3.fio = "Апанасенко Иосиф Родионович";
            a3.naz = "Украинец";
            a3.prof = "Дворник";
            och.Enqueue(a3);
            client a4;
            a4.fio = "Арватов Юрий Игнатьевич";
            a4.naz = "Молдованин";
            a4.prof = "Банкир";
            och.Enqueue(a4);
            client a5;
            a5.fio = "Балахонов Яков Филиппович";
            a5.naz = "Русский";
            a5.prof = "Полицейский";
            och.Enqueue(a5);
            client a6;
            a6.fio = "Балицкий Всеволод Аполлонович";
            a6.naz = "Русский";
            a6.prof = "Военный";
            och.Enqueue(a6);
            client a7;
            a7.fio = "Бахирев Александр Никифорович";
            a7.naz = "Украинец";
            a7.prof = "Менеджер";
            och.Enqueue(a7);
            client a8;
            a8.fio = "Богомолов Михаил Михайлович";
            a8.naz = "Башкир";
            a8.prof = "Повар";
            och.Enqueue(a8);
            client a9;
            a9.fio = "Воронов Михаил Александрович";
            a9.naz = "Украинец";
            a9.prof = "Мясник";
            och.Enqueue(a9);
            client a10;
            a10.fio = "Гарусский Михаил Петрович";
            a10.naz = "Белорус";
            a10.prof = "Тракторист";
            och.Enqueue(a10);
            while (och.Count > 0)
            {
                client v = (client)och.Dequeue();
                string s = $"ФИО - {v.fio} || Нац. - {v.naz} || Проф. - {v.prof}.";
                Console.WriteLine(s); 
            }
        }
    }
}
