﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabRectangle
{
    class Rectangle
    {
        private int _length;
        private int _width;

        public Rectangle()
        {
            _length = 1;
            _width = 1;
        }

        public Rectangle(int length, int width)
        {
            _length = length;
            _width = width;
        }

        public int GetArea()
        {
            return _length * _width;
        }
    }
}
