﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabRectangle
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle x = new Rectangle();
            Console.WriteLine(x.GetArea());

            Rectangle x2 = new Rectangle(5, 6);
            Console.WriteLine(x2.GetArea());

            Circle y = new Circle(8.95);
            Console.WriteLine(y.Area());
        }
    }
}
