﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ProektMillion
{
    public class danni
    {
        public string Vopros { get; set; }
        public string Otvet1 { get; set; }
        public string Otvet2 { get; set; }
        public string Otvet3 { get; set; }
        public string Otvet4 { get; set; }
        public int Pravo { get; set; }
        public string VivodVopros()
        {
            return Vopros;
        }
        public string VivodOtvet1()
        {
            return Otvet1;
        }
        public string VivodOtvet2()
        {
            return Otvet2;
        }
        public string VivodOtvet3()
        {
            return Otvet3;
        }
        public string VivodOtvet4()
        {
            return Otvet4;
        }
        public int VivodPravo()
        {
            return Pravo;
        }
    }
}
