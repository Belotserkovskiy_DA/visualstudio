﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabAnimal
{
    class Animal
    {
        public int Legs { get; protected set; }
    }
    class Pet : Animal
    {
        public string Fleas { get; protected set; }
        public string Name { get; set; }
        public int hair { get; set; }
        public Pet()
        {
            Legs = 4;
            Name = "Pitomec";
        }
    }
    class Cat: Pet
    {
        public Cat()
        {
            Name = "Koshak";
            Legs = 4;
        }
    }
    class Spider: Pet
    {
        public Spider()
        {
            Legs = 8;
            Fleas = "Spiders dont have fleas";
        }
    }
}
