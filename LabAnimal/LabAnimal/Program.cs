﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabAnimal
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat x1;
            x1 = new Cat();
            x1.Name = "Sharik";
            Console.WriteLine(x1.Name);
            Console.WriteLine(x1.Legs);
            Console.WriteLine("---------");

            Animal x;
            x = new Cat();
            Console.WriteLine(x.Legs);
            Cat xx = (Cat)x;
            Console.WriteLine(xx.Name);
            Console.WriteLine("\n");
            Console.WriteLine("My Little Spiders");
            Console.WriteLine("Poison is magic");
            Console.WriteLine("\n");
            Spider spi1;
            spi1 = new Spider();
            spi1.Name = "Arachna";
            spi1.hair = 9999;
            Console.WriteLine(spi1.Name + ", " + spi1.hair + " hairsprings, " + spi1.Legs + " legs" + " and " + spi1.Fleas);
            Spider spi2;
            spi2 = new Spider();
            spi2.Name = "BlackWidow";
            spi2.hair = 150;
            Console.WriteLine(spi2.Name + ", " + spi2.hair + " hairsprings, " + spi2.Legs + " legs" + " and " + spi2.Fleas);
            Spider spi3;
            spi3 = new Spider();
            spi3.Name = "Tarantul";
            spi3.hair = 645;
            Console.WriteLine(spi3.Name + ", " + spi3.hair + " hairsprings, " + spi3.Legs + " legs" + " and " + spi3.Fleas);
        }
    }
}
