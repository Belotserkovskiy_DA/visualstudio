﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabZapomniChislo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            start.Enabled = true;
        }
        int sloznost;
        int chisloo = 0;
        int verno = 0;
        int neverno = 0;


        private void Form1_Load(object sender, EventArgs e)
        {
            button1.BackColor = Color.Green;
            button2.BackColor = Color.Red;
        }
 
        private void gotovo_Click(object sender, EventArgs e)
        {
            starshiyBrat.Enabled = false;
            brat1.Enabled = false;
            brat2.Enabled = false;
            chet = 5;
                if (Convert.ToInt32(vvod.Text) == Convert.ToInt32(chislo.Text))
                {
                    verno++;
                    button1.Text = "Верно - " + verno;
                }
                else if (Convert.ToInt32(vvod.Text) != Convert.ToInt32(chislo.Text))
                {
                    neverno++;
                    button2.Text = "Неверно - " + neverno;
                }
                vvod.Text = "";
                vvod.Enabled = false;
                gotovo.Enabled = false;
            aa();
        }
        int returns = 0;
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(1);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            chislo.Visible = false;
            vivodchisla.Enabled = false;
            brat1.Enabled = true;
            starshiyBrat.Enabled = true;
            vvod.Enabled = true;
            gotovo.Enabled = true;
        }
        int aa()
        {
            if (sloznost == 1)
            {
                Random rnd = new Random();
                chisloo = rnd.Next(10000, 99999);
                chislo.Text = chisloo.ToString();
                vivodchisla.Enabled = true;
                chislo.Visible = true;
                vivodchisla.Interval = 3000;
            }
            else if (sloznost == 2)
            {
                Random rnd = new Random();
                chisloo = rnd.Next(100000, 999999);
                chislo.Text = chisloo.ToString();
                vivodchisla.Enabled = true;
                chislo.Visible = true;
            }
            else
            {
                Random rnd = new Random();
                chisloo = rnd.Next(1000000, 9999999);
                chislo.Text = chisloo.ToString();
                vivodchisla.Enabled = true;
                chislo.Visible = true;
                vivodchisla.Interval = 1000;
            }
            return returns;
        }
        private void timer2_Tick(object sender, EventArgs e)
        {
            vvod.Enabled = false;
            gotovo.Enabled = false;
            sloznost = Convert.ToInt32(peredatchik.Text);
            aa();
            start.Enabled = false;
        }
        double chet = 5;
        private void timer3_Tick(object sender, EventArgs e)
        {

                brat2.Enabled = true;
                brat1.Enabled = false;
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
          chet = chet - 0.1;
          label3.Text = chet.ToString();
          brat1.Enabled = true;
          brat2.Enabled = false;
            
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            chet = 5;
            brat1.Enabled = false;
            brat2.Enabled = false;
            gotovo.Enabled = false;
            vvod.Enabled = false;
            neverno++;
            button2.Text = "Неверно - " + neverno;
            vivodchisla.Enabled = true;
            chislo.Visible = true;
        }

        private void vvod_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                starshiyBrat.Enabled = false;
                brat1.Enabled = false;
                brat2.Enabled = false;
                chet = 5;
                if (Convert.ToInt32(vvod.Text) == Convert.ToInt32(chislo.Text))
                {
                    verno++;
                    button1.Text = "Верно - " + verno;
                }
                else if (Convert.ToInt32(vvod.Text) != Convert.ToInt32(chislo.Text))
                {
                    neverno++;
                    button2.Text = "Неверно - " + neverno;
                }
                vvod.Text = "";
                vvod.Enabled = false;
                gotovo.Enabled = false;
                aa();
            }
        }
    }
}
