﻿namespace LabZapomniChislo
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.vvod = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gotovo = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chislo = new System.Windows.Forms.Label();
            this.peredatchik = new System.Windows.Forms.Label();
            this.vivodchisla = new System.Windows.Forms.Timer(this.components);
            this.start = new System.Windows.Forms.Timer(this.components);
            this.brat1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.brat2 = new System.Windows.Forms.Timer(this.components);
            this.starshiyBrat = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(186, 63);
            this.button1.TabIndex = 0;
            this.button1.Text = "Верно - ";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(204, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(184, 63);
            this.button2.TabIndex = 1;
            this.button2.Text = "Неверно - ";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // vvod
            // 
            this.vvod.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vvod.Location = new System.Drawing.Point(12, 336);
            this.vvod.Name = "vvod";
            this.vvod.Size = new System.Drawing.Size(376, 20);
            this.vvod.TabIndex = 2;
            this.vvod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.vvod.KeyDown += new System.Windows.Forms.KeyEventHandler(this.vvod_KeyDown);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(376, 36);
            this.label1.TabIndex = 3;
            this.label1.Text = "Запомни число";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 277);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(376, 56);
            this.label2.TabIndex = 4;
            this.label2.Text = "Запиши число";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gotovo
            // 
            this.gotovo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gotovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gotovo.Location = new System.Drawing.Point(30, 362);
            this.gotovo.Name = "gotovo";
            this.gotovo.Size = new System.Drawing.Size(339, 55);
            this.gotovo.TabIndex = 5;
            this.gotovo.Text = "Готово";
            this.gotovo.UseVisualStyleBackColor = true;
            this.gotovo.Click += new System.EventHandler(this.gotovo_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.chislo);
            this.panel1.Location = new System.Drawing.Point(18, 122);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(370, 165);
            this.panel1.TabIndex = 7;
            // 
            // chislo
            // 
            this.chislo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chislo.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chislo.Location = new System.Drawing.Point(3, 0);
            this.chislo.Name = "chislo";
            this.chislo.Size = new System.Drawing.Size(364, 165);
            this.chislo.TabIndex = 0;
            this.chislo.Text = "000000";
            this.chislo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // peredatchik
            // 
            this.peredatchik.AutoSize = true;
            this.peredatchik.Location = new System.Drawing.Point(353, 87);
            this.peredatchik.Name = "peredatchik";
            this.peredatchik.Size = new System.Drawing.Size(0, 13);
            this.peredatchik.TabIndex = 8;
            this.peredatchik.Visible = false;
            // 
            // vivodchisla
            // 
            this.vivodchisla.Interval = 2000;
            this.vivodchisla.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // start
            // 
            this.start.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // brat1
            // 
            this.brat1.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(344, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 31);
            this.label3.TabIndex = 9;
            this.label3.Text = "5.0";
            // 
            // brat2
            // 
            this.brat2.Interval = 1;
            this.brat2.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // starshiyBrat
            // 
            this.starshiyBrat.Interval = 6200;
            this.starshiyBrat.Tick += new System.EventHandler(this.timer5_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 429);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.peredatchik);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gotovo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vvod);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.MinimumSize = new System.Drawing.Size(416, 468);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox vvod;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button gotovo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label chislo;
        public System.Windows.Forms.Label peredatchik;
        private System.Windows.Forms.Timer vivodchisla;
        private System.Windows.Forms.Timer start;
        private System.Windows.Forms.Timer brat1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Timer brat2;
        private System.Windows.Forms.Timer starshiyBrat;
    }
}

