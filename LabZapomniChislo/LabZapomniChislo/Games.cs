﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabZapomniChislo
{
    class Games
    {
        private Timer tm;

        public int CountCorrect { get; protected set; }
        public int CountWrong { get; protected set; }
        public int Code { get; protected set; }

        public event EventHandler GoScreenRemember;
        public event EventHandler GoScreenAnswer;
        public Games()
        {
            tm = new Timer();
            tm.Enabled = false;
            tm.Interval = 2000;
            tm.Tick += Event_Tick;
        }

        private void Event_Tick(object sender, EventArgs e)
        {
            tm.Enabled = false;
            if (GoScreenAnswer != null)
                GoScreenAnswer(this, EventArgs.Empty);
        }
    }

}
