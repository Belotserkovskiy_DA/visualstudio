﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace labforeach
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            int r = 0;
            foreach(int i in x)
            {
                r = r + i;
                Console.WriteLine(r);
            }
            Console.WriteLine();
            Console.WriteLine("sum = {0}", r);
            Console.WriteLine("_________________________________");
            string[] s = { "Bo", "Z", "he", " ", "m", "oy", " ", "kak", " ",  "bo", "l'", "no" };
            string k = "";
            foreach (string i in s)
            {
                k = k + i;
                Console.WriteLine(k);
            }
            Console.WriteLine();
            Console.WriteLine("Сентябрь = {0}", k);
        }
    }
}
