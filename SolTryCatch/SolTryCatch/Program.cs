﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabTryCatch
{
    class Program
    {
        static void Main(string[] args)
        {
            int y = 0;
            try
            {
                int[] z = { 1, 2, 3 };
                //int a = z[5];
                int x = 4 / y;
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Вы все сломали");

            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка - {0}", ex.Message);
            }
            finally
            {
                y = 123;
            }
            Console.WriteLine(y);
        }
    }
}
