﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilCs
{
    public class Class1
    {
        public static string Randomstr(int aLength, bool aLower, bool aUpper, bool aNumbr, bool aSpecl)
        {
            string c1 = "qazwsxedcrfvtgbyhnujmikolp";
            string c2 = "1234567890";
            string c3 = "[]{}<>,.;:-+#";

            var x = new StringBuilder();
            var xResult = new StringBuilder();
            Random rnd = new Random();

            if (aLower) x.Append(c1);
            if (aUpper) x.Append(c1.ToUpper());
            if (aNumbr) x.Append(c2);
            if (aSpecl) x.Append(c3);

            if (x.ToString() == "") x.Append(c1);

            while (xResult.Length < aLength)
                xResult.Append(x[rnd.Next(x.Length)]);
            return xResult.ToString();
        }
    }
}
