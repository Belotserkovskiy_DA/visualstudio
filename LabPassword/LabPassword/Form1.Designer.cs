﻿namespace LabPassword
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.niz = new System.Windows.Forms.CheckBox();
            this.verh = new System.Windows.Forms.CheckBox();
            this.numb = new System.Windows.Forms.CheckBox();
            this.spcl = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cifri = new System.Windows.Forms.NumericUpDown();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.cifri)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(12, 12);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(293, 38);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "<PASSWORD>";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(63, 56);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(190, 32);
            this.button1.TabIndex = 1;
            this.button1.Text = "СГЕНЕРИРОВАТЬ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // niz
            // 
            this.niz.AutoSize = true;
            this.niz.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.niz.Location = new System.Drawing.Point(12, 94);
            this.niz.Name = "niz";
            this.niz.Size = new System.Drawing.Size(229, 22);
            this.niz.TabIndex = 2;
            this.niz.Text = "Символы в нижнем регистре";
            this.niz.UseVisualStyleBackColor = true;
            // 
            // verh
            // 
            this.verh.AutoSize = true;
            this.verh.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.verh.Location = new System.Drawing.Point(12, 122);
            this.verh.Name = "verh";
            this.verh.Size = new System.Drawing.Size(233, 22);
            this.verh.TabIndex = 3;
            this.verh.Text = "Символы в верхнем регистре";
            this.verh.UseVisualStyleBackColor = true;
            // 
            // numb
            // 
            this.numb.AutoSize = true;
            this.numb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numb.Location = new System.Drawing.Point(12, 150);
            this.numb.Name = "numb";
            this.numb.Size = new System.Drawing.Size(78, 22);
            this.numb.TabIndex = 4;
            this.numb.Text = "Цифры";
            this.numb.UseVisualStyleBackColor = true;
            // 
            // spcl
            // 
            this.spcl.AutoSize = true;
            this.spcl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.spcl.Location = new System.Drawing.Point(12, 178);
            this.spcl.Name = "spcl";
            this.spcl.Size = new System.Drawing.Size(190, 22);
            this.spcl.TabIndex = 5;
            this.spcl.Text = "Специальные символы";
            this.spcl.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 223);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Число символов в пароле";
            // 
            // cifri
            // 
            this.cifri.Location = new System.Drawing.Point(210, 225);
            this.cifri.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.cifri.Name = "cifri";
            this.cifri.Size = new System.Drawing.Size(42, 20);
            this.cifri.TabIndex = 8;
            this.cifri.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // richTextBox1
            // 
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.Location = new System.Drawing.Point(311, 12);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(137, 233);
            this.richTextBox1.TabIndex = 9;
            this.richTextBox1.Text = "";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Red;
            this.button2.Location = new System.Drawing.Point(311, 221);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "ТЫК";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 256);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.cifri);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.spcl);
            this.Controls.Add(this.numb);
            this.Controls.Add(this.verh);
            this.Controls.Add(this.niz);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.MaximumSize = new System.Drawing.Size(484, 295);
            this.MinimumSize = new System.Drawing.Size(484, 295);
            this.Name = "Form1";
            this.Text = "Belotserkovsky DA";
            ((System.ComponentModel.ISupportInitialize)(this.cifri)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox niz;
        private System.Windows.Forms.CheckBox verh;
        private System.Windows.Forms.CheckBox numb;
        private System.Windows.Forms.CheckBox spcl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown cifri;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button2;
    }
}

