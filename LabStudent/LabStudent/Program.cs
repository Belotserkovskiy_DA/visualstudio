﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabStudent
{
    class Program
    {
        static void Main(string[] args)
        {
            Student x = new LabStudent.Student();
            x.ChangeAge += myEvent;
            x.Name = "Ivan";
            x.Surname = "Ivanovich";
            x.Age = 16;
            Console.WriteLine(x.GetFullName());
            x.Age = 18;
            Console.WriteLine(x.GetFullName());
            x.Age = 17;
            Console.WriteLine(x.GetFullName());
            Console.WriteLine("-------------------------------");

            Student student = new Student() { Name = "Misha", Surname = "Romanovich", Age = 23 };
            Console.WriteLine(student.GetFullName());
            Console.WriteLine("-------------------------------------------------------------------");
            Hero hero1 = new Hero() { Name = "Abaddon", MainChar = "Strength", Side = "Dire" };
            Console.WriteLine(hero1.GetStat());
            Hero hero2 = new Hero() { Name = "Razzil Darkbrew", MainChar = "Strength", Side = "Radiant" };
            Console.WriteLine(hero2.GetStat());
            Hero hero3 = new Hero() { Name = "Kaldr", MainChar = "Intelligency", Side = "Dire" };
            Console.WriteLine(hero3.GetStat());
            Hero hero4 = new Hero() { Name = "Magina", MainChar = "Agility", Side = "Radiant" };
            Console.WriteLine(hero4.GetStat());
            Hero hero5 = new Hero() { Name = "Mogul Khan", MainChar = "Strength", Side = "Dire" };
            Console.WriteLine(hero5.GetStat());
            Hero hero6 = new Hero() { Name = "Atropos", MainChar = "Intelligency", Side = "Dire" };
            Console.WriteLine(hero6.GetStat());
            Hero hero7 = new Hero() { Name = "Jin'zakk", MainChar = "Strength", Side = "Radiant" };
            Console.WriteLine(hero7.GetStat());
            Hero hero8 = new Hero() { Name = "Black Arachnia", MainChar = "Agility", Side = "Dire" };
            Console.WriteLine(hero8.GetStat());
            Hero hero9 = new Hero() { Name = "Bradwarden", MainChar = "Strength", Side = "Radiant" };
            Console.WriteLine(hero9.GetStat());

        }
        static public void myEvent(object sender, EventArgs myEvent)
        {
            Console.WriteLine("Izmenilsya vozrast");
        }
    }
}
