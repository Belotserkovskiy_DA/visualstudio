﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabStudent
{
    class Hero
    {
        public string Name { get; set; }
        public string MainChar { get; set; }
        public string Side { get; set; }
        public string GetStat()
        {
            return "Name - " + Name + " |MainStat - " + MainChar + " (" + Side + ")";
        }
    }
}
