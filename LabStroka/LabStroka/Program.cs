﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabStroka
{
    class Program
    {
        static void Main(string[] args)
        {
            int age = 860;
            int sword = 116;
            int crossbow = 83;
            string town = "Мордор";
            string name = "Милорд";
            string s1 = $"Привет,{name}! тебе уже {age} лет.";
            string s2 = $"{town} живет и процветает, местное время - {DateTime.Now:hh:mm}";
            string s3 = $"Орки выковали сегодня {sword} мечей и сделали {crossbow} арбалетa. Это хорошие показатели!";
            Console.WriteLine(s1);
            Console.WriteLine(s2);
            Console.WriteLine(s3);
        }
    }
}
