﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue x;
            x = new Queue();
            x.Enqueue(6);
            x.Enqueue(4);
            x.Enqueue(1);
            x.Enqueue(5);
            Console.WriteLine(x.Peek());
            Console.WriteLine("-------------------------");
            while (x.Count > 0 )
            {
                int y = (int)x.Dequeue();
                Console.WriteLine(y);
            }
        }
    }
}
